﻿using BlackJack.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJack.Services
{
    public class GameCommands
    {
        private Dictionary<int, Action<GameModel, int>> selectActionDictionary = new Dictionary<int, Action<GameModel, int>>();
        private Random random = new Random();
        private List<CardModel> playingCards;
        public GameCommands(){}
        public GameCommands(List<CardModel> playingCards)
        {
            selectActionDictionary.Add(1, StayingCommand);
            selectActionDictionary.Add(2, HittingCommand);
            selectActionDictionary.Add(3, DoubleDownCommand);
            selectActionDictionary.Add(4, SurrenderCardCommand);
            selectActionDictionary.Add(5, SplittingCommand);
            this.playingCards = playingCards;
        }
        public Dictionary<int, Action<GameModel, int>> GetSelectActionDictionary()
        {
            return selectActionDictionary;
        }
        public double ConvertInputValue(double convervalue,string NotificationMessage)
        {
            bool repeateLoopFlag = true;
            bool correctConvert = false;
            int resultConvering;

            
            while (repeateLoopFlag == true)
            {
                Console.WriteLine(NotificationMessage);
                correctConvert = Int32.TryParse(Console.ReadLine(), out resultConvering);
                if (correctConvert)
                {
                    convervalue = resultConvering;
                    repeateLoopFlag = convervalue == 0 ? true : false;
                }
            }
            return convervalue;
        }
        public CardModel GetRandomCard()
        {

            CardModel returnCard = playingCards[random.Next(playingCards.Count)];
            playingCards.Remove(returnCard);
            return returnCard;
        }

        public int GetScorePlayer(List<CardModel> playerCardLst)
        {
            int sum = 0;
            int aceWeightEleven = 11;
            int aceWeightOne = 1;
            foreach (var item in playerCardLst)
            {
                if (item.WeightCard == aceWeightOne)
                    sum += sum + aceWeightEleven > 21 ? 1 : 11;

                if(item.WeightCard != aceWeightOne)
                    sum += item.WeightCard;               
            }
            return sum;
        }

        public void SelectActionInGame(GameModel gameModel, int i)
        {
            Console.WriteLine("Select a command from 1 to 4: \n 1 - Staying \n 2 - Hitting \n 3 - Double Down \n 4 - Surrneder \n");

            int keyCommand = Convert.ToInt32(Console.ReadLine());
            selectActionDictionary[keyCommand].Invoke(gameModel, i);
        }

        public void WinCommand(GameModel gameModel, double factor)
        {
            gameModel.Player.CountMoney += gameModel.Player.Bet * factor;
            Console.WriteLine("--You Win");
            Console.WriteLine($"--Your money = {gameModel.Player.CountMoney}");
            PrintClass.PrintDillerResults(gameModel);
        }

        public void LoseCommand(GameModel gameModel, double factor, int i)
        {
            PrintClass.PrintDillerResults(gameModel);
            gameModel.Player.CountMoney -= gameModel.Player.Bet * factor;
            Console.WriteLine("--You Lose");
            gameModel.Player.PersonCards[i].Clear();
            Console.WriteLine($"--Your money = {gameModel.Player.CountMoney}");
        }

        public void CheckCondition(GameModel gameModel, Func<Boolean> exp, Action<GameModel> winAction, Action<GameModel> loseAction)
        {
            if (exp())            
                winAction(gameModel);

            if(!exp())
                loseAction(gameModel);            
        }

        public void StayingCommand(GameModel gameModel, int i)
        {
            Console.WriteLine("--------------------StayingCommand");
            PrintClass.PrintPlayerResults(gameModel, i);
            int player_score = this.GetScorePlayer(gameModel.Player.PersonCards[i]);
            int diller_score = this.GetScorePlayer(gameModel.Diller.PersonCards[0]);

            CheckCondition(gameModel, () => (player_score > diller_score), gmModel => WinCommand(gmModel, 1), gmModel => LoseCommand(gmModel, 1, i));

        }

        public void HittingCommand(GameModel gameModel, int i)
        {
            Console.WriteLine("--------------------HittingCommand");

            gameModel.Player.PersonCards[i].Add(this.GetRandomCard());
            PrintClass.PrintPlayerResults(gameModel, i);
            CheckCondition(gameModel, () => (this.GetScorePlayer(gameModel.Player.PersonCards[i]) <= 21), gmModel => SelectActionInGame(gmModel, i), gmModel => LoseCommand(gmModel, 1, i));

        }

        public void DoubleDownCommand(GameModel gameModel, int i)
        {
            Console.WriteLine("--------------------DoubleDownCommand");
            gameModel.Player.PersonCards[i].Add(this.GetRandomCard());
            PrintClass.PrintPlayerResults(gameModel, i);

            CheckCondition(gameModel, () => (this.GetScorePlayer(gameModel.Player.PersonCards[i]) <= 21), gmModel => WinCommand(gmModel, 2), gmModel => LoseCommand(gmModel, 2, i));
        }

        public void SplittingCommand(GameModel gameModel, int i)
        {
            Console.WriteLine("--------------------SplittingCommand");

            CardModel lastCard = gameModel.Player.PersonCards[0].Last();

            gameModel.Player.PersonCards.Add(1, new List<CardModel>());
            gameModel.Player.PersonCards[1].Add(lastCard);
            gameModel.Player.PersonCards[0].Remove(lastCard);
            gameModel.Player.PersonCards[0].Add(this.GetRandomCard());
            gameModel.Player.PersonCards[1].Add(this.GetRandomCard());

            PrintClass.PrintDillerResults(gameModel);
            Console.WriteLine("--------------Cards on first place");
            PrintClass.PrintPlayerResults(gameModel, 0);
            Console.WriteLine("--------------Cards on second place");
            PrintClass.PrintPlayerResults(gameModel, 1);
            int selectPlace;

            Console.WriteLine("Select the place you will play: \n 0 - first place \n 1 - second place");
            Int32.TryParse(Console.ReadLine(), out selectPlace);

            SelectActionInGame(gameModel, selectPlace);
            for (int j = 0; j < gameModel.Player.PersonCards.Count; j++)
            {
                if (gameModel.Player.PersonCards[j].Count > 0)
                {
                    Console.WriteLine("Select the place you will play: \n 0 - first place \n 1 - second place");
                    Console.WriteLine($"Selected place : {j}");
                    SelectActionInGame(gameModel, j);
                }
            }
        }

        public void SurrenderCardCommand(GameModel gameModel, int i)
        {

            Console.WriteLine("--------------------SurrenderCardCommand");
            PrintClass.PrintPlayerResults(gameModel, i);

            LoseCommand(gameModel, 0.5, i);
        }
    }
}
