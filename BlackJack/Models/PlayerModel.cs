﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace BlackJack.Models
{
    public class PlayerModel:GamePersonModel
    {
        private int bet;
        private GameModel gameModel;
        public PlayerModel(GameModel gameModel)
        {
            this.gameModel = gameModel;
        }
        public double CountMoney { get; set; }
        public int Bet
        {
            get{
                return bet;
            }
            set{
                bool firstCondition = value < 0 && value <= gameModel.MinBet && value >= gameModel.MaxBet;
                bool secondCondition = value >= this.CountMoney;

                if (firstCondition)
                    Console.WriteLine($"Bet must be more then Min Bet = {gameModel.MinBet} and less then Max Bet = {gameModel.MaxBet}");

                if (secondCondition)
                    Console.WriteLine($"Bet must be more then count your money");   
                           
                if(!firstCondition && !secondCondition)
                bet = value;
                // Contract.Requires(value >= this.CountMoney, $"Bet must be more then count your money");
            }
        }
    }
}
