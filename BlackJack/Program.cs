﻿using BlackJack.Models;
using BlackJack.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace BlackJack
{

    public class Program
    {       
        static void Main(string[] args)
        {
            GameMode newGame = new GameMode();
            newGame.StartGame();         
        }
    }
}
