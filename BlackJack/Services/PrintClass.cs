﻿using BlackJack.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJack.Services
{
    public static class PrintClass
    {
        
        public static void PrintPlayerResults(GameModel gameModel, int i)
        {
            Console.WriteLine("--Player cards");
            PrintPackCards(gameModel.Player.PersonCards[i]);
            Console.WriteLine($"--Score player card = {new GameCommands().GetScorePlayer(gameModel.Player.PersonCards[i])}");
        }
        public static void PrintPackCards(List<CardModel> cards)
        {
            foreach (var item in cards)
            {
                PrintCard(item);
            }
        }


        public static void PrintDillerResults(GameModel gameModel)
        {
            Console.WriteLine("--Diller cards");
            PrintPackCards(gameModel.Diller.PersonCards[0]);
            Console.WriteLine($"--Score Diller card = {new GameCommands().GetScorePlayer(gameModel.Diller.PersonCards[0])}");
        }
        public static void PrintCard(CardModel card)
        {
            Console.WriteLine(string.Format("{0} {1}", card.Card, card.Suit));
        }
    }
}
