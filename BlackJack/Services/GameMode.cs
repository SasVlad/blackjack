﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BlackJack.Models;

namespace BlackJack.Services
{
    public class GameMode
    {
        private List<CardModel> playingCards = new List<CardModel>();
        private int countPackCards = 1;

        public GameMode()
        {
            playingCards = new InitialCardPack().GetCardPack(countPackCards);
        }
        public void ResetGameData(GameModel gameModel)
        {
            gameModel.Player.PersonCards.Clear();
            gameModel.Diller.PersonCards.Clear();
            gameModel.Player.Bet = 0;
            playingCards = new InitialCardPack().GetCardPack(countPackCards);
        }
        public void StartGame()
        {
            GameCommands gameCommands = new GameCommands(playingCards);
            bool repeateLoopFlag = true;
            int splittingKey = 5;

            Console.WriteLine(new string('-', 24));
            Console.WriteLine("--Game Start");

            GameModel gameModel = new GameModel();

            gameModel.MinBet = 5;
            gameModel.MaxBet = 500;

            gameModel.Player.CountMoney = gameCommands.ConvertInputValue(gameModel.Player.CountMoney, $"Input count your money \n");

            while (repeateLoopFlag == true)
            {
                do
                {
                    gameModel.Player.Bet = (int)gameCommands.ConvertInputValue(gameModel.Player.Bet, $"Input your bet more then {gameModel.MinBet} and less then {gameModel.MaxBet} \n");
                } while (gameModel.Player.Bet == 0);

                gameModel.Player.PersonCards.Add(0, new List<CardModel>());
                gameModel.Diller.PersonCards.Add(0, new List<CardModel>());

                gameModel.Player.PersonCards[0].Add(gameCommands.GetRandomCard());
                gameModel.Player.PersonCards[0].Add(gameCommands.GetRandomCard());

                gameModel.Diller.PersonCards[0].Add(gameCommands.GetRandomCard());
                gameModel.Diller.PersonCards[0].Add(gameCommands.GetRandomCard());

                //-Print cards
                Console.WriteLine("--Player cards");
                PrintClass.PrintPackCards(gameModel.Player.PersonCards[0]);
                Console.WriteLine("--Diller cards");
                Console.WriteLine(new string('-', 12));//first diller card not visible for player
                PrintClass.PrintCard(gameModel.Diller.PersonCards[0].Last());

                Console.WriteLine($"--Score player card = {gameCommands.GetScorePlayer(gameModel.Player.PersonCards[0])}");
                Console.WriteLine($"--Score Diller card = {gameModel.Diller.PersonCards[0].Last().WeightCard}");


                if (gameCommands.GetScorePlayer(gameModel.Player.PersonCards[0]) == 21)
                    gameCommands.WinCommand(gameModel, 1.5);
                
                
                if (gameModel.Player.PersonCards[0][0].Card == gameModel.Player.PersonCards[0][1].Card)
                    gameCommands.GetSelectActionDictionary()[splittingKey].Invoke(gameModel, 1);
                    
                if(gameModel.Player.PersonCards[0][0].Card != gameModel.Player.PersonCards[0][1].Card)
                    gameCommands.SelectActionInGame(gameModel, 0);

               ResetGameData(gameModel);

                    

                Console.WriteLine("Try Again? [Y|N]");
                ConsoleKey key = Console.ReadKey().Key;

                if (key == ConsoleKey.Y)                   
                   repeateLoopFlag = true;
                    
                if (key == ConsoleKey.N)               
                   repeateLoopFlag = false;
                   

                }
            }
        }
    
}
