﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJack
{
    public class CardModel
    {
        public Cards Card { get; set; }
        public Suits Suit { get; set; }
        public int WeightCard { get; set; }
    }
}
