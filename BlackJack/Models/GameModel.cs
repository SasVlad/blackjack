﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJack.Models
{
    public class GameModel
    {
        public GameModel()
        {
            Player = new PlayerModel(this);
            Diller = new DillerModel();
        }
        public int MinBet { get; set; }
        public int MaxBet { get; set; }
        public PlayerModel Player { get; set; }
        public DillerModel Diller { get; set; }
    }
}
