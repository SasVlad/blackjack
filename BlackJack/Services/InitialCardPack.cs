﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJack
{
    public class InitialCardPack
    {
        public List<CardModel> GetCardPack(int countPacks)
        {
            List<CardModel> playingCards = new List<CardModel>();

            Array arrayUniqCards = Enum.GetValues(typeof(Cards));
            Array arrayUniqSuits = Enum.GetValues(typeof(Suits));
            while (countPacks!=0)
            {
                for (int i = 0; i < arrayUniqSuits.Length; i++)
                {
                    for (int j = 0; j < arrayUniqCards.Length; j++)
                    {
                        Cards currentCard = (Cards)arrayUniqCards.GetValue(j);
                        playingCards.Add(
                            new CardModel
                            {
                                Card = currentCard,
                                Suit = (Suits)arrayUniqSuits.GetValue(i),
                                WeightCard = currentCard == Cards.Ten || currentCard == Cards.Jack || currentCard == Cards.Queen || currentCard == Cards.King ? 10 : j + 1
                            });
                    }
                }
                countPacks -= 1;
            }
            
            return playingCards;       
        }
    }
}
