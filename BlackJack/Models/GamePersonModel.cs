﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJack.Models
{
    public class GamePersonModel
    {
        public GamePersonModel()
        {
            PersonCards = new Dictionary<int, List<CardModel>>();
        }
        public Dictionary<int,List<CardModel>> PersonCards { get; set; }
        public int PersonScore { get; set; }
    }
}
